from openerp import models, fields, api, _
import datetime

import logging
_logger = logging.getLogger(__name__)

class developer_tweaks_module(models.Model):
    # _name = 'developer_tweaks.module'
    _inherit = 'ir.module.module'
    _order = 'write_date desc'
    _description = "Developer Tweaks's Custom Module"

    @api.model
    def dt_dummy(self):
        _logger.debug('UPGRADE')
        return self._button_immediate_function(type(self).button_upgrade)

    @api.multi
    def do_immediate_upgrade(self):
        return self._button_immediate_function(type(self).button_upgrade)

    @api.multi
    def button_immediate_upgrade(self):
        return super(developer_tweaks_module, self)._button_immediate_function(type(self).button_upgrade)
        # return self._button_immediate_function(type(self).button_upgrade)

    # overwrite super method
    @api.multi
    def _button_immediate_function(self, function):
        # import pdb;
        # pdb.set_trace();
        _logger.debug('_button_immediate_function')
        super(developer_tweaks_module, self)._button_immediate_function(function)

        _logger.debug('relooooooad')
        # reload on current page instead redirecting to the first available root menu
        return {
            'type': 'ir.actions.client',
            'tag': 'reload'
        }
